package com.ex.school;

import com.ex.school.Student;

public class Main {


    /**
     *
     * Student
     * Teacher
     * Employee - Director, Administrator, etc
     *
     */
    public static void main(String[] args) {
        // write your code here

        Teacher t1 = new Teacher(
                1234567890567L,
                "Maria",
                "Popescu",
                46,
                "Engleză"
        );

        System.out.println(t1.toString());

        Student s1 = new Student(
                1237654323456L,
                "Ion",
                "Antonescu",
                22,
                8.7
        );

        System.out.println(s1.toString());


        Employee e1 = new Employee(
                24567898765L,
                "Dorian",
                "Dragomir",
                61,
                "Director"
        );

        System.out.println(e1.toString());
    }


}
